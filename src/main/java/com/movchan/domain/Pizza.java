package com.movchan.domain;

public class Pizza {
    private int id = 0;
    private String name;
    private int price;

    public Pizza(String name, int price) {
        this.name = name;
        this.price = price;
        id++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
