package com.movchan;

import com.movchan.domain.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;


@WebServlet("/users/*")
public class MyServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(MyServlet.class);
    private static Map<Integer, Pizza> pizzaMap = new LinkedHashMap<>();

    @Override
    public void init() throws ServletException {
        LOGGER.info("Servlet " + this.getServletName() + " has started");
        Pizza pizza = new Pizza("Cesare", 200);
        Pizza pizza1 = new Pizza("Cheese", 180);
        Pizza pizza2 = new Pizza("Palermo", 210);
        pizzaMap.put(pizza.getId(), pizza);
        pizzaMap.put(pizza1.getId(), pizza1);
        pizzaMap.put(pizza2.getId(), pizza2);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("Into doGet");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h2> Pizza List</h2>");
        for (Pizza p : pizzaMap.values()) {
            out.println("<p>"+ p+ "</p>");
        }
        out.println("<form action='users' method='POST'>\n"
                + "Pizza Name: <input type='text' name='pizza_name'> \n"
                + "Price: <input type='text' name='pizza price'> \n"
                + "<button type='submit'>Send to server</button>\n" +
                "   </form>");
        out.println("<form>\n"
                + "<p><b> DELETE ELEMENT </b></p>\n"
                +"<p> Pizza id: <input type = 'text' name = 'pizza_id'>\n"
                +"<input type= 'button' onclick ='remove(this.form.pizza_id.value)' name ='ok' value = 'Delete element'"
                + "</p>\n"
                + "</form>");
        out.println("<script type = 'text/javascript'>\n"
                + "function remove(id) { fetch('users/'+ id, {method:'DELETE'}); }\n "
                + "</script>");
        out.println("<p>Request URI: " + request.getRequestURI() + "</p>");
        out.println("<p>Method: " + request.getMethod() + "</p>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("Into do post() " + request.getParameter("info"));
        String newPizzaName = request.getParameter("pizza_name");
        int newPizzaAddress = Integer.parseInt(request.getParameter("pizza_address"));
        LOGGER.info("add " + newPizzaName);
        Pizza newPizza = new Pizza(newPizzaName, newPizzaAddress);
        pizzaMap.put(newPizza.getId(), newPizza);
        doGet(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doDelete");
        String id = req.getRequestURI();
        LOGGER.info(" URL = " + id);
        id = id.replace("/servlets-1.0/users/","");
        LOGGER.info("id = " + id);
        pizzaMap.remove(Integer.parseInt(id));
         doGet(req, resp);
    }

    @Override
    public void destroy() {
        LOGGER.info("Servlet " + this.getServletName() + " has stopped");
    }
}
